package FitNesse.RemoteRunner.TeamCity.Plugin.Server;

import jetbrains.buildServer.log.Loggers;
import jetbrains.buildServer.serverSide.BuildServerAdapter;
import jetbrains.buildServer.serverSide.BuildServerListener;
import jetbrains.buildServer.serverSide.SBuildServer;
import jetbrains.buildServer.util.EventDispatcher;
import org.jetbrains.annotations.NotNull;
import FitNesse.RemoteRunner.TeamCity.Plugin.Common.Util;

public class ServerListener extends BuildServerAdapter {
  private SBuildServer buildServer;

  public ServerListener(@NotNull final EventDispatcher<BuildServerListener> dispatcher, SBuildServer server) {
    dispatcher.addListener(this);
    this.buildServer = server;
  }

  @Override
  public void serverStartup() {
    Loggers.SERVER.info("Plugin '" + Util.NAME + "'. Is running on server version " + this.buildServer.getFullServerVersion() + ".");
  }
}
