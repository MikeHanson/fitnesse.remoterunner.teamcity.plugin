package FitNesse.RemoteRunner.TeamCity.Plugin.Server;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jetbrains.buildServer.controllers.BaseController;
import jetbrains.buildServer.web.openapi.PluginDescriptor;
import jetbrains.buildServer.web.openapi.WebControllerManager;
import org.springframework.web.servlet.ModelAndView;
import FitNesse.RemoteRunner.TeamCity.Plugin.Common.Util;

public class Controller extends BaseController {
    private PluginDescriptor pluginDescriptor;

    public Controller(PluginDescriptor pluginDescriptor, WebControllerManager manager) {
        this.pluginDescriptor = pluginDescriptor;
        manager.registerController("/FitNesseRemoteRunner.html", this);
    }

    @Override
    protected ModelAndView doHandle(final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        ModelAndView view = new ModelAndView(pluginDescriptor.getPluginResourcesPath("FitNesseRemoteRunner.jsp"));
        final Map model = view.getModel();
        model.put("name", Util.NAME);
        return view;
    }
}
