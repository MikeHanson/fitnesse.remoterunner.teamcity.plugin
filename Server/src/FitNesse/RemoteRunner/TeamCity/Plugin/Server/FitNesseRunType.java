package FitNesse.RemoteRunner.TeamCity.Plugin.Server;

import FitNesse.RemoteRunner.TeamCity.Plugin.Common.Util;
import jetbrains.buildServer.serverSide.PropertiesProcessor;
import jetbrains.buildServer.serverSide.RunType;
import jetbrains.buildServer.serverSide.RunTypeRegistry;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class FitNesseRunType extends RunType {

    Map<String, String> defaultProperties = new HashMap<String, String>();


    public FitNesseRunType(final RunTypeRegistry runTypeRegistry) {
        runTypeRegistry.registerRunType(this);
    }

    @NotNull
    @Override
    public String getType() {
        return Util.RUNNER_TYPE;
    }

    @Override
    public String getDisplayName() {
        return "FitNesse Remote Runner";
    }

    @Override
    public String getDescription() {
        return "Plugin for running FitNesse tests via the REST API";
    }

    @Override
    public PropertiesProcessor getRunnerPropertiesProcessor() {
        return new FitNessePropertiesProcessor();
    }

    @Override
    public String getEditRunnerParamsJspFilePath() {
        return "editFitnesse.jsp";
    }

    @Override
    public String getViewRunnerParamsJspFilePath() {
        return "viewFitnesse.jsp";
    }

    @Override
    public Map<String, String> getDefaultRunnerProperties() {
        return defaultProperties;
    }
}
