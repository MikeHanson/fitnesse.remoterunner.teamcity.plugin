<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>

<l:settingsGroup title="Fitnesse options">
    <tr>
        <th>
            <label for="fitnesseHost">FitNesse Host: </label>
        </th>
        <td>
            <props:textProperty name="fitnesseHost"/>
            <span class="error" id="error_fitnesseHost"></span>
            <span class="smallNote">localhost</span>
        </td>
    </tr>
    <tr>
        <th>
            <label for="fitnessePort">Port: </label>
        </th>
        <td>
            <props:textProperty name="fitnessePort"/>
            <span class="error" id="error_fitnessePort"></span>
            <span class="smallNote">8080</span>
        </td>
    </tr>
    <tr>
        <th>
            <label for="fitnesseTest">Test or Suite Page: </label>
        </th>
        <td>
            <props:textProperty name="fitnesseTest"/>
            <span class="error" id="error_fitnesseTest"></span>
            <span class="smallNote">TestSuite.SubSuite</span>
        </td>
    </tr>
    <tr>
        <th>
            <label for="fitnesseTestType">Test Type: </label>
        </th>
        <td>
            <props:selectSectionProperty name="fitnesseTestType" title="Type of page Suite or Test:">
                <props:selectSectionPropertyContent value="suite" caption="Suite"/>
                <props:selectSectionPropertyContent value="test" caption="Test"/>
            </props:selectSectionProperty>
            <span class="error" id="error_fitnesseTest"></span>
            <span class="smallNote">Select the type of the test page</span>
        </td>
    </tr>


</l:settingsGroup>
