package FitNesse.RemoteRunner.TeamCity.Plugin.Agent;

import FitNesse.RemoteRunner.TeamCity.Plugin.Common.Util;
import jetbrains.buildServer.agent.AgentRunningBuild;
import jetbrains.buildServer.agent.BuildFinishedStatus;
import jetbrains.buildServer.agent.BuildProgressLogger;
import jetbrains.buildServer.agent.BuildRunnerContext;
import org.jetbrains.annotations.NotNull;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;


public class FitNesseProcess extends FutureBasedBuildProcess {

    @NotNull
    private final AgentRunningBuild Build;
    @NotNull
    private final BuildRunnerContext Context;
    @NotNull
    private final BuildProgressLogger Logger;

    public FitNesseProcess(@NotNull final AgentRunningBuild build, @NotNull final BuildRunnerContext context) {
        Build = build;
        Context = context;
        Logger = build.getBuildLogger();
    }

    private String getParameter(@NotNull final String parameterName) {
        return getParameter(parameterName, null);
    }

    private String getParameter(@NotNull final String parameterName, String defaultValue) {
        final String value = Context.getRunnerParameters().get(parameterName);
        if (value == null || value.trim().length() == 0) return defaultValue;
        String result = value.trim();
        return result;
    }

    public boolean getSuiteResults(URL testUrl) throws MalformedURLException {
        InputStream inputStream = null;
        String suiteName = "FitNesse " + getTestPage();
        try {
            Logger.progressMessage("Connnecting to " + testUrl);
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();
            Logger.progressMessage("Connected: " + connection.getResponseCode() + "/" + connection.getResponseMessage());

            inputStream = connection.getInputStream();

            XMLEventReader xmlReader = XMLInputFactory.newInstance().createXMLEventReader(inputStream);

            Integer rightCount = 0;
            Integer wrongCount = 0;
            Integer ignoresCount = 0;
            Integer exceptionsCount = 0;
            Integer runTimeInMillis = 0;
            String relativePageName = "";
            String pageHistoryLink = "";

            Logger.logSuiteStarted(suiteName);

            while (xmlReader.hasNext()) {
                XMLEvent event = xmlReader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    String elName = startElement.getName().getLocalPart();

                    if (elName.equalsIgnoreCase("result")) {
                        rightCount = 0;
                        wrongCount = 0;
                        ignoresCount = 0;
                        exceptionsCount = 0;
                        runTimeInMillis = 0;
                        relativePageName = "";
                        pageHistoryLink = "";
                    } else if (elName.equalsIgnoreCase("right")) {
                        event = xmlReader.nextEvent();
                        String data = event.asCharacters().getData();
                        rightCount = Integer.parseInt(data);
                    } else if (elName.equalsIgnoreCase("wrong")) {
                        event = xmlReader.nextEvent();
                        String data = event.asCharacters().getData();
                        wrongCount = Integer.parseInt(data);
                    } else if (elName.equalsIgnoreCase("ignores")) {
                        event = xmlReader.nextEvent();
                        String data = event.asCharacters().getData();
                        ignoresCount = Integer.parseInt(data);
                    } else if (elName.equalsIgnoreCase("exceptions")) {
                        event = xmlReader.nextEvent();
                        String data = event.asCharacters().getData();
                        exceptionsCount = Integer.parseInt(data);
                    } else if (elName.equalsIgnoreCase("runTimeInMillis")) {
                        event = xmlReader.nextEvent();
                        String data = event.asCharacters().getData();
                        runTimeInMillis = Integer.parseInt(data);
                    } else if (elName.equalsIgnoreCase("relativePageName")) {
                        event = xmlReader.nextEvent();
                        relativePageName = event.asCharacters().getData();
                    } else if (elName.equalsIgnoreCase("pageHistoryLink")) {
                        event = xmlReader.nextEvent();
                        pageHistoryLink = event.asCharacters().getData();
                    }
                } else if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equalsIgnoreCase("result")) {
                        String testName = pageHistoryLink;
                        if ((rightCount == 0) && (wrongCount == 0) && (exceptionsCount == 0)) {
                            Logger.logTestIgnored(testName, "empty test");
                        } else {
                            Logger.logTestStarted(testName, new Date(System.currentTimeMillis() - runTimeInMillis));

                            if ((wrongCount > 0) || (exceptionsCount > 0)) {
                                Logger.logTestFailed(testName, String.format("wrong:%d  exception:%d", wrongCount, exceptionsCount), "");
                            }

                            Logger.logTestFinished(testName, new Date());
                        }
                    }
                }
            }
            xmlReader.close();
        } catch (Exception e) {
            Logger.exception(e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                }
            }
            Logger.logSuiteFinished(suiteName);
        }
        return true;

    }

    private String getHost() {
        return getParameter(Util.PROPERTY_FITNESSE_HOST, "localhost");
    }

    private String getTestType() {
        return getParameter(Util.PROPERTY_FITNESSE_TEST_TYPE, "suite");
    }

    private int getPort() {
        return Integer.parseInt(getParameter(Util.PROPERTY_FITNESSE_PORT));
    }

    private URL getTestAbsoluteUrl() throws MalformedURLException {
        return new URL(String.format("http://%s:%d/%s?%s&format=xml", getHost(), getPort(), getTestPage(), getTestType()));
    }

    private String getTestPage() {
        return getParameter(Util.PROPERTY_FITNESSE_TEST);
    }

    @NotNull
    public BuildFinishedStatus call() throws Exception {

        if (getTestPage().isEmpty()) {
            Logger.message("Nothing to run");
            return BuildFinishedStatus.FINISHED_SUCCESS;
        }

        try {
            URL testUrl = this.getTestAbsoluteUrl();

            getSuiteResults(testUrl);

            Logger.progressMessage("terminating");

            return BuildFinishedStatus.FINISHED_SUCCESS;
        } catch (Exception e) {
            Logger.exception(e);
            return BuildFinishedStatus.FINISHED_FAILED;
        }
    }
}
