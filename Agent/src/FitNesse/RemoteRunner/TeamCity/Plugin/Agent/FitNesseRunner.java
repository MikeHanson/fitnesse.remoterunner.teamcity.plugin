package FitNesse.RemoteRunner.TeamCity.Plugin.Agent;

import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.*;
import org.jetbrains.annotations.NotNull;

import FitNesse.RemoteRunner.TeamCity.Plugin.Common.Util;

public class FitNesseRunner implements AgentBuildRunner, AgentBuildRunnerInfo {

    @NotNull
    public BuildProcess createBuildProcess(@NotNull final AgentRunningBuild runningBuild, @NotNull final BuildRunnerContext context) throws RunBuildException
    {
        return  new FitNesseProcess(runningBuild, context);
    }

    @NotNull
    public AgentBuildRunnerInfo getRunnerInfo()
    {
        return this;
    }

    @NotNull
    public String getType() {
        return Util.RUNNER_TYPE;
    }

    public boolean canRun(@NotNull BuildAgentConfiguration agentConfiguration) {
        return true;
    }
}
