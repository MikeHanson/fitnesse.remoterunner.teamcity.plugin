package FitNesse.RemoteRunner.TeamCity.Plugin.Common;

import java.util.Collection;
import java.util.Iterator;

public class Util {
    public final static String NAME = "FitNesse Remote Runner";
    public final static String RUNNER_TYPE = "FitNesseRemoteRunner";

    public final static String PROPERTY_FITNESSE_HOST = "fitnesseHost";
    public final static String PROPERTY_FITNESSE_PORT = "fitnessePort";
    public final static String PROPERTY_FITNESSE_TEST = "fitnesseTest";
    public final static String PROPERTY_FITNESSE_TEST_TYPE = "fitnesseTestType";


    public static String join(Collection<?> s, String delimiter) {
        StringBuilder builder = new StringBuilder();
        Iterator iterator = s.iterator();
        while (iterator.hasNext()) {
            builder.append(iterator.next());
            if (!iterator.hasNext()) {
                break;
            }
            builder.append(delimiter);
        }
        return builder.toString();
    }
}
